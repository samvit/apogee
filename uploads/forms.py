from django import forms
from apogee.uploads.models import PaperUpload,ProjectUpload, VirtuosityUpload
class PaperUploadForm(forms.ModelForm):
    class Meta:
        model = PaperUpload
        exclude = ('participant', 'finalized', )

class ProjectUploadForm(forms.ModelForm):
    class Meta:
        model = ProjectUpload
        exclude = ('participant', 'finalized', )

class VirtuosityUploadForm(forms.ModelForm):
    class Meta:
        model = VirtuosityUpload
        exclude = ('participant', 'finalized', )