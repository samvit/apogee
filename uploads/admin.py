from apogee.uploads.models import PaperUpload,ProjectCategory,ProjectUpload,PaperCategory,VirtuosityUpload
from django.contrib import admin
from apogee.events.models import Event

def finalize_papers(modeladmin, request, queryset):
    event = Event.objects.get(name='Paper Presentation')
    for paper in queryset:
        if event not in paper.participant.shortlisted_events.all():
            paper.participant.shortlisted_events.add(event)
        paper.finalized = True
        paper.save()
finalize_papers.short_description = 'Finalize paper uploads'

def finalize_projects(modeladmin, request, queryset):
    event = Event.objects.get(name='Prototype: Project Presentation')
    for project in queryset:
        if event not in project.participant.shortlisted_events.all():
            project.participant.shortlisted_events.add(event)
        project.participant.shortlisted_events.add(event)
        project.finalized = True
        project.save()
finalize_projects.short_description = 'Finalize project uploads'


class PaperUploadAdmin(admin.ModelAdmin):
    list_display = ['participant','filename','category','finalized']
    list_filter = ['category','finalized']
    readonly_fields = ['category','participant']
    actions = [finalize_papers]
    
    def finalize(self, request, queryset):
        rows_updated = queryset.update(finalized = True)
        if rows_updated == 1:
            message_bit = "1 Paper was"
        else:
            message_bit = "%s Papers were" % rows_updated
        self.message_user(request, "%s shortlisted for on campus Paper Presentation Apogee 2012." % message_bit)
    finalize.short_description = "Mark selected as Shortlisted for on campus Paper Presentation"

    def reset_status(self, request, queryset):
        rows_updated = queryset.update(finalized = None)
        if rows_updated == 1:
            message_bit = "Status of 1 Paper was"
        else:
            message_bit = "Status of %s Papers were" % rows_updated
        self.message_user(request, "%s reset " % message_bit)
    reset_status.short_description = "Reset Finalised status"
    
    #actions = [finalize,reset_status]
    search_fields = ['name','participant']
    def filename(self,up):
        return str(up.paper).split('/')[-1]

class ProjectUploadAdmin(admin.ModelAdmin):
    list_display = ['participant','filename','category','finalized']
    list_filter = ['category']
    readonly_fields = ['category','participant']
    actions = [finalize_projects]
    def filename(self,up):
        return str(up.project).split('/')[-1]


		
class VirtuosityAdmin(admin.ModelAdmin):
	list_display = ['participant','file','name']
admin.site.register(PaperUpload,PaperUploadAdmin)
admin.site.register(PaperCategory)
admin.site.register(ProjectUpload,ProjectUploadAdmin)
admin.site.register(ProjectCategory)
admin.site.register(VirtuosityUpload,VirtuosityAdmin)