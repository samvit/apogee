from django.db import models
from apogee.registration.models import Participant

# Create your models here.

class PaperCategory(models.Model):
    name = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.name



class PaperUpload(models.Model):
    name = models.CharField('Title of the paper', max_length=300)
    category = models.ForeignKey(PaperCategory)
    paper = models.FileField(upload_to = 'paperuploads', blank = True)
    participant = models.ForeignKey(Participant)
    finalized = models.NullBooleanField(blank=True, null=True)

    def __unicode__(self):
        return self.name

class ProjectUpload(models.Model):
    name = models.CharField('Title of the project', max_length=300)
    category = models.ForeignKey('ProjectCategory')
    project = models.FileField(upload_to = 'projectuploads', blank = True)
    participant = models.ForeignKey(Participant)
    finalized = models.NullBooleanField(blank=True, null=True)

    def __unicode__(self):
        return self.name

class ProjectCategory(models.Model):
    name = models.CharField(max_length = 100)

    def __unicode__(self):
        return self.name
        
class VirtuosityUpload(models.Model):
    name = models.CharField(max_length=300)
    file = models.FileField(upload_to='virtuosityuploads')
    participant = models.ForeignKey(Participant)
    finalized = models.BooleanField()

    def __unicode__(self):
        return self.name
