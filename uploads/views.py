# Create your views here.
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect,Http404,HttpResponse
from django.template import RequestContext
from apogee.registration.models import Participant
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from apogee.registration.models import Participant
from apogee.uploads.models import PaperUpload,ProjectUpload,PaperCategory, VirtuosityUpload
from apogee.uploads.forms import PaperUploadForm,ProjectUploadForm, VirtuosityUploadForm
from django.core.exceptions import ObjectDoesNotExist
from apogee.events.models import Event
import xlwt
import hashlib

@login_required
def virtuosityupload(request):
    try :
        stu = Participant.objects.get(pk = request.user.id)
        e = Event.objects.get(name='Virtuosity')
    except ObjectDoesNotExist:
        return render_to_response('viruploads.html',{ 'message' : 'You need to be logged in'},context_instance=RequestContext(request))
    if not e in stu.events.all():
        m = 'You need to register for the Virtuosity event first'
        return render_to_response('viruploads.html', {'message': m},context_instance=RequestContext(request))
    if request.POST:
        form = VirtuosityUploadForm(request.POST,request.FILES)
        if form.is_valid():
            if request.FILES.has_key('file'):

                if str(request.FILES['file'].name).split('.')[-1] not in ['docx','doc','txt','pdf', 'rar', 'zip']:
                    form.errors['file'] = 'Only doc,docx,txt,pdf are allowed'
                    return render_to_response('viruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))

                if request.FILES['file'].size > 3*1024*1024:
                    form.errors['file'] = 'File size exeeds 3MB, Please upload a smaller file'
                    return render_to_response('viruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
                p = VirtuosityUpload()

                p.participant = stu
                p.name = form.cleaned_data['name']
                p.file = request.FILES['file']
                p.file.name = stu.username + '-' + p.file.name
                p.save()

                return render_to_response('viruploads.html',{'form' : form, 'p' : stu,'message' : 'Your file has been uploaded'},context_instance=RequestContext(request))
            else :
                form.errors['file'] = 'Please upload a file'
                return render_to_response('viruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
        return render_to_response('viruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
    form = VirtuosityUploadForm()
    return render_to_response('viruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))

@login_required
def delvirtuosityupload(request):
    if request.POST:
        if request.POST.has_key('id'):
            try :
                upload = VirtuosityUpload.objects.get( pk = request.POST['id'])
                upload.delete()
                return HttpResponse('Your file has been deleted')
            except ObjectDoesNotExist:
                return HttpResponse('Some error has occured!')
    return HttpResponse('Some error has occured!')

@login_required
@csrf_protect
def paperupload(request):

    try :
        stu = Participant.objects.get(pk = request.user.id)
        e = Event.objects.get(name='Paper Presentation')
    except ObjectDoesNotExist:
        form = PaperUploadForm()
        return render_to_response('uploads.html', {'message': 'You need to be logged in'},context_instance=RequestContext(request))
    if not e in stu.events.all():
        m = 'You need to register for the <a target="blank" href="http://www.bits-apogee.org/2012/Papers_&_Projects/Paper_Presentation/">Paper Presentation</a> event first'
        return render_to_response('uploads.html', {'message': m},context_instance=RequestContext(request))
    if request.POST:
        form = PaperUploadForm(request.POST,request.FILES)
        if form.is_valid():
            if request.FILES.has_key('paper'):
                try:
                    if str(request.FILES['paper'].name).split('.')[-1] not in ['docx','doc','txt','pdf']:
                        form.errors['paper'] = 'Only doc,docx,txt,pdf are allowed.'
                        return render_to_response('uploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
                except:
                    form.errors['paper'] = 'Some error in your filename has occurred. Please rename your file.'
                    return render_to_response('uploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))

                if request.FILES['paper'].size > 3*1024*1024:
                    form.errors['paper'] = 'File size exceeds 3MB, Please upload a smaller file.'
                    return render_to_response('uploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
                p = PaperUpload()
                p.category = form.cleaned_data['category']
                p.name = form.cleaned_data['name']
                p.participant = stu
                p.paper = request.FILES['paper']
                p.paper.name = '%s-%s-%s' % (p.category, stu.username, p.paper.name)
                p.save()

                return render_to_response('uploads.html',{'form' : form, 'p' : stu,'message' : 'Your file has been uploaded'},context_instance=RequestContext(request))
            else :
                form.errors['paper'] = 'Please upload a file'
                return render_to_response('uploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
        return render_to_response('uploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
    form = PaperUploadForm()
    return render_to_response('uploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))


@login_required
def delpaperupload(request):

    if request.POST:
        if request.POST.has_key('id'):
            try :
                paper = PaperUpload.objects.get( pk = request.POST['id'])
                paper.delete()
                return HttpResponse('Your file has been deleted')
            except ObjectDoesNotExist:
                return HttpResponse('Some error has occured!')
    return HttpResponse('Some error has occured!')



@login_required
@csrf_protect
def projectupload(request):

    try :
        stu = Participant.objects.get( pk = request.user.id)
       
    except ObjectDoesNotExist:
        return render_to_response('pruploads.html',{ 'message' : 'You need to be logged in'},context_instance=RequestContext(request))
    e = Event.objects.get(pk='242')
    if not e in stu.events.all():
        m = 'You need to register for the Project Presentation event first'
        return render_to_response('pruploads.html', {'message': m},context_instance=RequestContext(request))
    if request.POST:
        form = ProjectUploadForm(request.POST,request.FILES)
        if form.is_valid():
            if request.FILES.has_key('project'):

                if str(request.FILES['project'].name).split('.')[-1] not in ['docx','doc','txt','pdf']:
                    form.errors['project'] = 'Only doc,docx,txt,pdf are allowed'
                    return render_to_response('pruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))

                if request.FILES['project'].size > 3*1024*1024:
                    form.errors['project'] = 'File size exeeds 3MB, Please upload a smaller file'
                    return render_to_response('pruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
                p = ProjectUpload()
                p.category = form.cleaned_data['category']

                p.participant = stu
                p.name = form.cleaned_data['name']
                p.project = request.FILES['project']
                try :
	                p.project.name = '%s-%s-%s' % (p.category, stu.username, str(p.project.name))
	        except :
	        	p.project.name =  'AbstractAPOGE2012'
                p.save()

                return render_to_response('pruploads.html',{'form' : form, 'p' : stu,'message' : 'Your file has been uploaded'},context_instance=RequestContext(request))
            else :
                form.errors['project'] = 'Please upload a file'
                return render_to_response('pruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
        return render_to_response('pruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))
    form = ProjectUploadForm()
    return render_to_response('pruploads.html',{'form' : form, 'p' : stu},context_instance=RequestContext(request))


@login_required
def delprojectupload(request):
    print 'Im here'
    if request.POST:
        if request.POST.has_key('id'):
            try :
                project = ProjectUpload.objects.get( pk = request.POST['id'])
                project.delete()
                return HttpResponse('Your file has been deleted')
            except ObjectDoesNotExist:
                return HttpResponse('Some error has occured!')
    return HttpResponse('Some error has occured!')

def paperxl(request):
	categs = PaperCategory.objects.all()
	book = xlwt.Workbook()
	for cat in categs:
		sheet = book.add_sheet(cat.name)
		style = xlwt.easyxf('font: name Times New Roman, color-index blue, bold on')
		sheet.write(0,0,'Name of Paper',style = style)
		sheet.write(0,1,'Category',style = style)
		sheet.write(0,2,'Participant name',style = style)
		sheet.write(0,3,'College',style = style)
		sheet.write(0,4,'E-mail',style = style)
		sheet.write(0,5,'Contact Number',style = style)
		sheet.write(0,6,'Apogee ID',style = style)
                ups = PaperUpload.objects.filter(category__name = cat.name)
                row = 1
                for up in ups:
                        sheet.write(row,0,str(up.paper).split('/')[-1])
                        sheet.write(row,1,str(cat.name)) 
                        sheet.write(row,2,up.participant.get_full_name())
                        try :
                            sheet.write(row,3,str(up.participant.college)) #+ ', ' + up.participant.college.place)
                        except :
                            
                            sheet.write(row,3,'BITS,Pilani,Pilani Campus')
                        sheet.write(row,4,up.participant.email)
                        sheet.write(row,5,up.participant.contact_no)
                        sheet.write(row,6,hashlib.sha224(str(up.participant.id)).hexdigest()[10:17])
                        row+=1
        response = HttpResponse(mimetype='application/vnd.ms-excel')
        response['Content-Disposition'] = 'attachment; filename=paperxl.xls'
        book.save(response)
        return response
        
def papers(request):
	categs = PaperCategory.objects.all()
	s=''
	for cat in categs:
		ups = PaperUpload.objects.filter(category__name = cat.name)
		for up in ups:
			s+=str(up.paper) + '<br/>'
	return HttpResponse(s)


		
		
	