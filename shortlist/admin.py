from django.contrib import admin
from apogee.registration.models import Participant
from apogee.shortlist.models import Shortlist
from apogee.events.models import Event
from apogee.shortlist.forms import ShortlistAdminForm
from django.template.loader import render_to_string
from django.core.mail import send_mail
import hashlib
class ShortlistAdmin(admin.ModelAdmin):
    form = ShortlistAdminForm
    list_display = ('desc','created_on' , 'author','finalized')
    filter_horizontal = ['participants']
    list_filter = ('event', 'author')
    actions = ['delete','finalise']
    def finalized(self,shortlist):
      for participant in shortlist.participants.all():
        print participant.finalized
        if participant.finalized == ( False or None ) :
          return 'No'
      return 'Yes'
       
    def preprocess_list_display(self, request):
      if not request.user.is_superuser:
        self.list_display.remove('author')
        #self.list_display.remove('Finalised')

    def changelist_view(self, request, extra_context=None):
        self.preprocess_list_display(request)
        return super(ShortlistAdmin, self).changelist_view(request)

    def finalise(self, request, queryset):
        rows_updated = 0
        for shortlist in queryset:
          for participant in shortlist.participants.all():
            participant.finalized = True
            participant.save()
            apid = hashlib.sha224(str(participant.id)).hexdigest()[:7]
            workshops = participant.shortlisted_events.filter(category__name = 'Workshops')
	    astro = workshops.filter(name = 'Astro Workshop')
            events = participant.shortlisted_events.exclude(category__name = 'Workshops')
            team = events.filter(is_team = True)
	    paper = events.filter(name = 'Paper Presentation')
            email = render_to_string('confirm_mail.html',{ 'p' : participant , 'id' : apid ,'workshops':workshops,'events':events,'team':team ,'paper':paper,'astro':astro })
            send_mail('Apogee 2011 Confirmation',email,'Apogee 2011 <noreply@bits-apogee.org>',[participant.email])
            rows_updated+=1
        if rows_updated == 1:
            message_bit = "1 Participant was"
        else:
            message_bit = "%s Participants were" % rows_updated
        self.message_user(request, "%s successfully finalised for Apogee 2011. Emails sent." % message_bit)
    finalise.short_description = "Mark selected as finalised"


    def desc(self,s):
        return 'Shortlisted participants for Event "'+ str(s.event) + '"'

    def save_model(self,request,obj,form,change):
        if getattr(obj,'author',None) is None:
            obj.author = request.user
        obj.last_modified_by = request.user
        if form.cleaned_data['sall']:
          participants = Participant.objects.filter(events = form.cleaned_data['event'])
          form.save(commit=False)
          form.cleaned_data['participants'] = participants
          form.save()
          for participant in participants:
            participant.shortlisted_events.add(obj.event)
        else:
          for participant in form.cleaned_data['participants']:
            participant.shortlisted_events.add(obj.event)
        obj.save()
        
    def delete(self, request, queryset):
        for shlist in queryset:
            for p in  shlist.participants.all():
                p.shortlisted_events.remove(shlist.event)
        queryset.delete()
        self.message_user(request, "Shortlist deleted." )
    delete.short_description = "Delete selected shortlists"

    def get_actions(self, request):
        actions = super(ShortlistAdmin, self).get_actions(request)
        del actions['delete_selected']
	if request.user.is_superuser:
          return actions
        else:
          del actions['finalise']
          return actions
        
    def queryset(self , request):
        qs = super(ShortlistAdmin , self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author = request.user)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
	if request.user.is_superuser:
            return super(ShortlistAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        if db_field.name == "event":
            kwargs["queryset"] = Event.objects.filter(author = request.user,register = True)
        return super(ShortlistAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
	if request.user.is_superuser:
             return super(ShortlistAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)
        if db_field.name == "participants":
            e = Event.objects.filter(author = request.user,register = True)
            kwargs["queryset"] = Participant.objects.filter(events__in = e).distinct()

        return super(ShortlistAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

admin.site.register(Shortlist,ShortlistAdmin)
