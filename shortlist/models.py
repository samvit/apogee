from django.db import models
from apogee.registration.models import Participant
from apogee.events.models import Event
from django.contrib.auth.models import User
# Create your models here.

class Shortlist(models.Model):
    event = models.ForeignKey(Event)
    participants = models.ManyToManyField(Participant,related_name = 'Part')
    author = models.ForeignKey(User,null = True , blank = True)
    created_on = models.DateTimeField(auto_now_add = True)

    def __unicode__(self):
        return 'Shortlisted participants for Event "'+ str(self.event) + '"'