from django import forms
from django.contrib.formtools.wizard import FormWizard
from apogee.shortlist.models import Shortlist
from apogee.events.models import Event
from apogee.registration.models import Participant
from django import forms
'''
class ShortlistFormE(forms.Form):
    desc = forms.CharField(max_length = 300,label = 'Description')
    event = forms.ChoiceField(choices = Event.objects.filter(author = request.user,register = True))


class ShortlistFormP(forms.Form,event):
      participant = forms.MultipleChoiceField(choices = Participant.objects.filter(event = e).distinct())

class ShortlistForm(FormWizard):
    @property
    def __name__(self):
        return self.__class__.__name__

    def done(self , request,form_list):
        data = {}
        for form in form_list:
            data.update(form.cleaned_data)'''


class ShortlistAdminForm(forms.ModelForm):
    class Meta:
        model = Shortlist
        exclude = ('author')
    sall = forms.BooleanField(label = 'Shortlist all participants in selected event',required = False)
    def clean_participants(self):

        data = self.cleaned_data['participants']
	if not self.cleaned_data.has_key('event'):
            raise forms.ValidationError('Please select an event')
        for participant in data:
            events = participant.events.all()
            sel_event = Event.objects.get(name__exact = str(self.cleaned_data['event']))
            if sel_event not in events:
                raise forms.ValidationError("Participant '"+str(participant.username)+"' not registered with event '" + str(self.cleaned_data['event'])+"'")
        return data
