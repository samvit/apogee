from django import forms

class JoinForm(forms.Form):
    team_id = forms.CharField(max_length = 10)



class CreateForm(forms.Form):
    event = forms.ModelChoiceField( queryset = '')

    def __init__(self, events, *args, **kwargs):
        super(CreateForm, self).__init__(*args, **kwargs)
        self.fields['event'].queryset = events


