from django.db import models
from apogee.events.models import Event
from apogee.registration.models import Participant

class Team(models.Model):
    event = models.ForeignKey(Event,related_name = 'team_event')
    members = models.ManyToManyField(Participant,related_name = 'team_members')
    id_tag = models.CharField(max_length = 30)
    admin = models.ForeignKey(Participant,related_name = 'team_admin')
    max = models.IntegerField(null = True)
    finalized = models.NullBooleanField()

    def __unicode__(self):
        return str(self.event)
