# Create your views here.
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect,Http404,HttpResponse
from django.template import RequestContext
from apogee.registration.models import Participant
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from apogee.teamreg.models import Team
from apogee.events.models import Event,EventCategory
from apogee.teamreg.forms import JoinForm,CreateForm
from hashlib import sha1
from django.core.urlresolvers import reverse
from apogee.updates.models import Update

@login_required
@csrf_protect
def team_create(request):
    cats  = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    p = Participant.objects.get(pk = request.user.id)
    if request.POST:
        if request.POST.has_key('event'):
           options = Participant.objects.get( pk = request.user.id).events.filter(is_team = True)
           form = CreateForm(options,request.POST)
           if form.is_valid():
               event = Event.objects.get( pk = request.POST['event'])


               if p.team_admin.filter(event = event).count():
                form.errors['event'] = 'You have already created a team for this event'
                return render_to_response('teamreg.html',{'form':form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))

               if p.team_members.filter(event = event).count():
                form.errors['event'] = 'You are already a member of a team for this event'
                return render_to_response('teamreg.html',{'form':form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))

               new = Team()
               new.event = event
               new.admin = p
               new.save()
               t = sha1(str(new.id))

               new.id_tag = t.hexdigest()[:5]
               new.max = event.max_participants
               new.save()
               new.members.add(p)
               return render_to_response('profile.html',{'success': 'Your team has been created, Check the My Teams tab','cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))
           else :
            options = Participant.objects.get( pk = request.user.id).events.filter(is_team = True)
            form.errors['event'] = 'This field is required'
            return render_to_response('teamreg.html',{'form':form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))
    options = Participant.objects.get( pk = request.user.id).events.filter(is_team = True)
    form = CreateForm(options)
    return render_to_response('teamreg.html',{'form':form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))


@login_required
@csrf_protect
def join_team(request):
    cats  = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    form = JoinForm(request.POST)
    try :
        p = Participant.objects.get(pk = request.user.id)
    except ObjectDoesNotExist:
        raise Http404
    if request.POST:
       if form.is_valid():
        try :
            team = Team.objects.get( id_tag = request.POST['team_id'])
        except ObjectDoesNotExist:
            form.errors['team_id'] = 'Team does not exist, recheck Team id'
            return render_to_response('jointeam.html',{'form':form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))

        if team.event not in p.events.all():
           form.errors['team_id'] = 'You are not registered for this event'
           return render_to_response('jointeam.html',{'form': form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))
        if p.team_members.filter(event = team.event).count():
           form.errors['team_id'] = 'You are already a member of a team for this event '
           return render_to_response('jointeam.html',{'form': form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))
        if len(team.members.all()) >= team.max :
           form.errors['team_id'] = 'This team is full'
           return render_to_response('jointeam.html',{'form': form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))
        team.members.add(p)
        return render_to_response('profile.html',{'success': 'You have been added to the team, Check the My Teams tab','cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))

       else :
        form.errors['team_id'] = 'This field is required'
        return render_to_response('jointeam.html',{'form':form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))
    form = JoinForm()
    return render_to_response('jointeam.html',{'form': form,'cats' : cats,'p':p,'updates':updates},context_instance=RequestContext(request))

@login_required
def remove_me(request):
    try :
        p = Participant.objects.get(pk = request.user.id)
    except ObjectDoesNotExist:
        return HttpResponse('You need to be logged in as a Participant')
    if request.POST:
       try :
           team = Team.objects.get( pk = request.POST['id'])
       except :
        return HttpResponse('Opps some error has occured')
       team.members.remove(p)
       return HttpResponse('You have been removed from the Team')
    return HttpResponse('Opps some error has occured')


def webteam(request):
  cats  = EventCategory.objects.all().order_by('weight')
  return render_to_response('webteam.html',{'cats' : cats})




