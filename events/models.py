from django.db import models
from django.contrib.auth.models import User
#mport gatekeeper

# Create your models here.
positions = (
          ('left', 'Left-menu'),
          ('right' , 'Right-menu'),
          )
class EventCategory(models.Model):
    name = models.CharField(max_length = 50)
    weight = models.IntegerField(help_text= 'Heavier items sink to the bottom of the menu.')
    icon = models.ImageField(upload_to='images/icons')
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Event Categories'



class Tag(models.Model):
    name = models.CharField(max_length = 15)
    def __unicode__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(max_length = 100,verbose_name = 'Event Name' ,unique = True)
    category = models.ForeignKey(EventCategory)
#    tags = models.ManyToManyField(Tag)
    overview = models.TextField(blank = 'True')
    contact = models.TextField(blank = 'True')
    attachments = models.FileField(upload_to = 'files',blank='True')
    author = models.ForeignKey(User, null = True , blank = True)
    register = models.BooleanField(verbose_name = 'Enable online registration')
    is_team = models.BooleanField( verbose_name = 'Team event')
    max_participants = models.IntegerField(null = True,blank = True)
    facebook_admin_id = models.CharField( max_length = 100 , null = True,blank = True,help_text = 'You can find your facebook id at graph.facebook.com/< your facebook username >, access admin page on your FB -> Account->Manage Pages')
    weight = models.IntegerField( null = True, blank = True)

    def __unicode__(self):
        return self.name



class Heading(models.Model):
    name = models.CharField(max_length = 60,unique = True)
    weight = models.IntegerField()

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Tab headings'
        verbose_name = 'Tab heading'


class Tabs(models.Model):
    heading = models.ForeignKey(Heading)
    content = models.TextField(blank = True)
    event = models.ForeignKey(Event)

    def __unicode__(self):
        return 'Tab for '+ self.event.name

    class Meta:
        verbose_name_plural = 'Tabs'
        verbose_name = 'Tab'


