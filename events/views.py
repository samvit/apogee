# Create your views here.

#from django.contrib import simplejson
from apogee.events.models import Event
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect,Http404,HttpResponse
from django.utils import simplejson
from apogee.sponsors.models import SponsorUpload
from apogee.events.forms import WorkshopForm
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.db import IntegrityError
from apogee.updates.models import Update
from apogee.sponsors.models import SponsorUpload,Category
from apogee.events.models import EventCategory,Tabs
from django.core.mail import send_mail
from apogee.registration.models import Participant
from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist
def home(request):
    cats  = EventCategory.objects.all().order_by('weight')
    try:
    	event = Event.objects.get(name='About')
	event.overview = event.overview.replace('\'', '\\\'')
	event.overview = event.overview.replace('\n', '')
    	event.overview = event.overview.replace('\r', '')
    except:
	raise Http404
    updates = Update.objects.all().order_by('-timestamp')
    return render_to_response("base.html",{ 'cats' : cats,'event':event ,'updates':updates },context_instance=RequestContext(request))

def view404(request):
    cats  = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    return render_to_response("404.html",{ 'cats' : cats,'updates':updates },context_instance=RequestContext(request))


def home_view(request,event):
    event = event.replace('_', ' ')
    try:
    	event = Event.objects.get(name=event)
	if event.name == 'Sponsors':
            s =  SponsorUpload.objects.all().order_by('category__weight')
            c = Category.objects.all().order_by('weight')
            event.overview = render_to_string('sponsor.html',{'sponsors': s , 'categories' : c })

    except:
	raise Http404
    cats = EventCategory.objects.all().order_by('weight')
    tabs =  event.tabs_set.all().order_by('heading__weight')
    for tab in tabs :
        tab.content = tab.content.replace('\n','')
        tab.content = tab.content.replace('\r', '')
        tab.content = tab.content.replace('\'', '\\\'')
    event.overview = event.overview.replace('\'', '\\\'')
    event.overview = event.overview.replace('\n', '')
    event.overview = event.overview.replace('\r', '')
    updates = Update.objects.all()
    return render_to_response("base.html", {'cats':cats, 'event':event, 'tabs':tabs,'updates':updates}, context_instance=RequestContext(request))
       

def display_event(request, category, event):
    event = event.replace('_', ' ')
    try:
    	event = Event.objects.get(name=event)
	if event.name == 'Sponsors':
	    s =  SponsorUpload.objects.all().order_by('category__weight')
            c = Category.objects.all().order_by('weight')
            event.overview = render_to_string('sponsor.html',{'sponsors': s , 'categories' : c })

    except:
	raise Http404
    cats = EventCategory.objects.all().order_by('weight')

    tabs =  event.tabs_set.all().order_by('heading__weight')
    for tab in tabs :
        tab.content = tab.content.replace('\n','')
        tab.content = tab.content.replace('\r', '')
        tab.content = tab.content.replace('\'', '\\\'')
    event.overview = event.overview.replace('\'', '\\\'')
    event.overview = event.overview.replace('\n', '')
    event.overview = event.overview.replace('\r', '')
    updates = Update.objects.all()
    return render_to_response("base.html", {'cats':cats, 'event':event,'updates':updates ,'tabs':tabs}, context_instance=RequestContext(request))

def getevents(request):
    if request.method == 'GET':
        data = request.GET
        if data.has_key('cat'):
            es = []
	    
            events = Event.objects.filter(category__name__exact=data['cat']).order_by('weight')
            for event in events:
                es.append(event.name)
            json = simplejson.dumps(es)
            return HttpResponse(json, mimetype='application/json')
        else:
            raise Http404

def getcontent(request):
    if request.method == 'GET':
        data = request.GET
        if data.has_key('even'):
            event_details= {}
            try :
                event = Event.objects.get(name=data['even'])
            except ObjectDoesNotExist:
                return Http404

            event_details['id'] = event.id
            event_details['name'] = event.name
            if event.contact:
                event_details['overview'] = event.overview + '<br/>For further details contact : <br/>'+event.contact
            else:
                event_details['overview'] = event.overview
	    if event.name == 'Sponsors':
            	s =  SponsorUpload.objects.all().order_by('category__weight')
	        c = Category.objects.all().order_by('weight')
        	event_details['overview'] = render_to_string('sponsor.html',{'sponsors': s , 'categories' : c })

            if request.user.is_authenticated():
                try :
                    p = Participant.objects.get(pk = request.user.id).events.all()
                    if event in p:
                        event_details['register']  = -1
                    else:
                        event_details['register']  = event.register
                except ObjectDoesNotExist:
                    event_details['register'] = event.register
            else :
                 event_details['register']  = event.register
            tabs = Tabs.objects.filter(event = event).order_by('heading__weight')
            ts = {}
            for tab in tabs:
                ts[tab.heading.name] = tab.content
            event_details['tabs'] = ts
            event_details['category'] = event.category.name
            json = simplejson.dumps(event_details)
            return HttpResponse(json, mimetype='application/json')

    raise Http404

def getimg(request):
    s = SponsorUpload.objects.all().order_by('category__weight')
    sponsors = []
    for sponsor in s:
        sponsors.append({'name': sponsor.name,'link':sponsor.link,'category':str(sponsor.category),'src': str(sponsor.image)})
    json = simplejson.dumps(sponsors)
    return HttpResponse(json)



def contacts(request):
    cats  = EventCategory.objects.all().order_by('weight')
    return render_to_response('contacts.html',{ 'cats' : cats},context_instance=RequestContext(request))


def facebook(request,fb):
    try :
        cat = Event.objects.get(name__exact = fb)
    except ObjectDoesNotExist:
        raise Http404

    return render_to_response('events.html',{'fb':fb , 'event' : cat})


def categories(request):
   categs  = EventCategory.objects.all().order_by('weight')
   cats = {}
   for cat in categs:
    cats[cat] = '0'
   return  cats
@csrf_protect
def workshop(request):
    notice = ''
    if request.POST:
       form = WorkshopForm(request.POST)
       if form.is_valid():
           message = form.cleaned_data['name'] +'\n'+'\n'+ form.cleaned_data['college'] +'\n'+ '\n'+form.cleaned_data['workshop_details'] +'\n'+'\n'+ form.cleaned_data['city'] +'\n'+'\n'+ form.cleaned_data['contact_no'] +'\n'+ '\n'+form.cleaned_data['e_mail']
           send_mail('Workshops Apogee 2011', message , 'noreply@bits-apogee.org',['pranjal.kalra@gmail.com','samvit.1@gmail.com'])
           notice = 'Thank you! We will get back to you shorltly'
    else:
        form = WorkshopForm()
        notice = ''
    sponsor_links = SponsorUpload.objects.values('image','width')
    cats  = EventCategory.objects.all().order_by('weight')
    return render_to_response('workshop.html',{ 'cats' : cats, 'sponsors': sponsor_links , 'form' : form , 'notice' : notice},context_instance=RequestContext(request))

def participation(request):
    cats  = EventCategory.objects.all().order_by('weight')
    return render_to_response("participation.html",{ 'cats' : cats },context_instance=RequestContext(request))


def downloads(request):
  cats  = EventCategory.objects.all().order_by('weight')
  return render_to_response("downloads.html",{ 'cats' : cats },context_instance=RequestContext(request))

def search(request):
  cats  = EventCategory.objects.all().order_by('weight')
  if request.GET.has_key('event'):
    if request.GET['event']!= '': 
      events = Event.objects.filter(name__icontains = request.GET['event'])
      return render_to_response('search.html',{'events':events,'cats' : cats },context_instance=RequestContext(request))
  return render_to_response('search.html',{ 'cats' : cats },context_instance=RequestContext(request))

"""
	MOBILE SITE VIEWS

"""

def mob_index(request):
    try:
        cats = EventCategory.objects.all().order_by('weight')
    except EventCategory.DoesNotExist:
        raise Http404
    return render_to_response('mob_index.html',{ 'cats' : cats },context_instance=RequestContext(request))

def mob_events(request, event):
    try:
        events = Event.objects.filter(category__name__exact=event).order_by('weight')
    except Event.DoesNotExist:
        raise Http404
    return render_to_response('mob_events.html',{ 'events' : events, 'cat': event },context_instance=RequestContext(request))

def mob_eventPage(request, eventPage):
    try:
        e = Event.objects.get(name=eventPage)
    except Event.DoesNotExist:
        raise Http404
    tabs = Tabs.objects.filter(event = e).order_by('heading__weight')
    details = {}
    for tab in tabs:
        details[tab.heading.name] = tab.content
    return render_to_response('mob_eventPage.html',{ 'e' : e, 'details' : details },context_instance=RequestContext(request))
