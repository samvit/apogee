from django import forms
from django.contrib.auth.models import User
from apogee.events.models import Event,Tabs
from tinymce.widgets import TinyMCE


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        widgets = {
                'overview' : TinyMCE(attrs={'cols': 120, 'rows': 20 }),
                'contact' : TinyMCE(attrs={'cols': 30, 'rows': 10}) ,
                }
        exclude = ('author','register',)

class EventAdminForm(forms.ModelForm):
    class Meta:
        model = Event
        widgets = {
                'overview' : TinyMCE(attrs={'cols': 120, 'rows': 20 }),
                'contact' : TinyMCE(attrs={'cols': 30, 'rows': 10}) ,
                }
class TabsAdminForm(forms.ModelForm):
    class Meta:
        model = Tabs
        widgets = {
                'content' : TinyMCE(attrs={'cols': 120, 'rows': 20}),
        }
c = (
('Joomla' ,'Joomla'),
('Animation','Animation'),
('Hacking','Hacking'),
)
class WorkshopForm(forms.Form):
    name = forms.CharField(max_length = 100)
    college = forms.CharField(max_length = 100)
    city = forms.CharField(max_length = 50)
    workshop_details = forms.CharField(widget=forms.Select(choices = c))
    contact_no = forms.CharField(max_length = 100)
    e_mail = forms.EmailField()
