from apogee.events.models import Event,EventCategory,Tag,Tabs,Heading
from django.contrib import admin
from apogee.events.forms import EventAdminForm,TabsAdminForm,EventForm
from django.core.mail import send_mail

class TabsInline(admin.StackedInline):
    form = TabsAdminForm
    model = Tabs
    extra = 2

class EventAdmin(admin.ModelAdmin):
    def get_form(self, request, obj=None, **kwargs):
        if request.user.is_superuser:
            return EventAdminForm
        else:
            return EventForm
    def save_model(self,request,obj,form,change):
        if getattr(obj,'author',None) is None:
            obj.author = request.user
        obj.last_modified_by = request.user
        obj.save()
	if not request.user.is_superuser:
            message = 'User : ' + str(request.user.username) + '  changed Event "' + str(form.cleaned_data['name']) + '"'
            send_mail('Apogee 2012 Event Content change notifications', message , 'noreply@bits-apogee.org',['samvit.1@gmail.com'], fail_silently=True)
    def queryset(self , request):
        qs = super(EventAdmin , self).queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author = request.user)
    list_display = ( 'name','register','author' ,'category',)
    inlines = [ TabsInline , ]
    search_fields = ['name',]
    list_filter = ('category',)


class EventCategoryAdmin(admin.ModelAdmin):
    list_display = ('name','weight',)
    list_editable = ('weight',)

class TabsAdmin(admin.ModelAdmin):
    form = TabsAdminForm
    def queryset(self,request):
	qs = super(TabsAdmin,self).queryset(request)
	if request.user.is_superuser:
	    return qs
	return qs.filter(event__author = request.user)

admin.site.register(Event,EventAdmin)
admin.site.register(EventCategory,EventCategoryAdmin)
admin.site.register(Tag)
admin.site.register(Tabs,TabsAdmin)
admin.site.register(Heading)

