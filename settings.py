# Django settings for apogee project.

import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG

APPEND_SLASH = True

ADMINS = (
   ('Samvit Majumdar', 'samvit.1@gmail.com'),
   
)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'mysql',
        'NAME': 'apogee_2012',
        'USER': 'apogee_apogee',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '3306',                    
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

LANGUAGE_CODE = 'en-us'

SITE_ID = 1

USE_I18N = True

USE_L10N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media').replace('\\', '/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/2012/site_media/'

ADMIN_MEDIA_PREFIX = '/2012/site_media/a_media/'

SECRET_KEY = '3$%*nrelz^ayxugud_ohs6_97m)-$&$46g!i5$82!2-0wexlwr'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)
TEMPLATE_CONTEXT_PROCESSORS = (
	'django.core.context_processors.auth',
    'django.contrib.auth.context_processors.auth',
	'django.core.context_processors.debug',
	'django.core.context_processors.i18n',
	'django.core.context_processors.media',

	'django.contrib.messages.context_processors.messages',
	'django.core.context_processors.request',
)
MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

FILE_UPLOAD_PERMISSIONS = 0644

ROOT_URLCONF = 'apogee.urls'

TINYMCE_JS_URL = '/2012/site_media/js/tiny_mce/tiny_mce.js'
TINYMCE_JS_ROOT = '/2012/site_media/js/tiny_mce'
TINYMCE_DEFAULT_CONFIG ={ 'theme' : 'advanced', 'relative_urls': False, }

TEMPLATE_DIRS = (
    os.path.join(os.path.dirname(__file__), 'media').replace('\\', '/'),
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'apogee.registration.auth_backend.ParticipantModelBackend',
)
LOGIN_URL = '/2012/login/'

AJAX_LOOKUP_CHANNELS = {
                     'college' : ('apogee.registration.ajax' , 'SelectCollege')
}
AJAX_SELECT_BOOTSTRAP = True
AJAX_SELECT_INLINES = 'inline'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    
    
    'admin_tools',
    'admin_tools.theming',
    'admin_tools.menu',
    'admin_tools.dashboard',
     'django.contrib.admin',
     'apogee.events',
     'tinymce',
     'apogee.sponsors',

     'apogee.registration',
     'ajax_select',
#     'apogee.shortlist',
     'apogee.uploads',
     'apogee.teamreg',
     'apogee.updates',
     'apogee.bus_reservation',
)

ADMIN_TOOLS_INDEX_DASHBOARD = 'apogee.dashboard.CustomIndexDashboard'
ADMIN_TOOLS_APP_INDEX_DASHBOARD = 'apogee.dashboard.CustomAppIndexDashboard'
