from django import forms
from apogee.registration.models import Participant
from ajax_select.fields import AutoCompleteSelectMultipleField, AutoCompleteSelectField
from django.contrib.auth.models import User
import re

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length = 30 , help_text = ' ')
    #password = forms.CharField(max_length = 30, widget = forms.PasswordInput())
    #re_type_password = forms.CharField(max_length = 30,widget = forms.PasswordInput())
    first_name = forms.CharField(max_length = 100)
    last_name = forms.CharField(max_length = 100)
    e_mail = forms.EmailField()
    gender = forms.CharField( widget = forms.RadioSelect(choices = ( ('Male'  , 'Male') ,('Female' , 'Female'),)))
    college = AutoCompleteSelectField('college',label = 'College*',help_text='If you can\'t find the name of your college, try entering the place instead (e.g., chennai)')
    contact_no = forms.IntegerField()
    year = forms.ChoiceField(choices = (('1', 'First Year'), ('2', 'Second Year'),('3', 'Third Year'),('4', 'Fourth Year'), ('5', 'Fifth Year'), ('6', 'Post Graduate')))


    def clean_username(self):
        username = self.cleaned_data.get('username')
        if re.match(r'^[_A-Za-z0-9]+$', username) is None:
            raise forms.ValidationError('Invalid username. Username may contain alphanumeric character and underscores only.')
        return username

    def clean_e_mail(self):
        e_mail = self.cleaned_data.get('e_mail')
        username = self.cleaned_data.get('username')
        if e_mail and User.objects.filter(email=e_mail).count() > 0:
            raise forms.ValidationError('This email address is already registered.')
        return e_mail

    def clean_contact_no(self):
        number = self.cleaned_data.get('contact_no')
        if number <= 0:
            raise forms.ValidationError('Invalid phone number. Phone numbers must be exactly ten digits long.')
        elif len(str(number)) != 10:
            raise forms.ValidationError('Please enter a valid phone number exactly ten digits long.')
        return number


class WorkshopForm(forms.Form):
    name = forms.CharField(max_length = 100)
    college = forms.CharField(max_length = 100)
    city = forms.CharField(max_length = 50)
    workshop_details = forms.CharField(widget=forms.Textarea)
    contact_no = forms.CharField(max_length = 100)
    e_mail = forms.EmailField()

class ForgotForm(forms.Form):
    username = forms.CharField(max_length = 100)

class EditProfileForm(forms.Form):
    first_name = forms.CharField(max_length = 100)
    last_name = forms.CharField(max_length = 100)
    e_mail = forms.EmailField()
    contact_no = forms.IntegerField()
    year = forms.ChoiceField(choices = (('1', 'First Year'), ('2', 'Second Year'),('3', 'Third Year'),('4', 'Fourth Year'), ('5', 'Fifth Year'), ('6', 'Post Graduate')))
    
    def clean_contact_no(self):
        number = self.cleaned_data.get('contact_no')
        if number <= 0:
            raise forms.ValidationError('Invalid phone number. Phone numbers must be exactly ten digits long.')
        elif len(str(number)) != 10:
            raise forms.ValidationError('Please enter a valid phone number exactly ten digits long.')
        return number
