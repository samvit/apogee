from django.contrib.auth.backends import ModelBackend
from apogee.registration.models import Participant

class ParticipantModelBackend(ModelBackend):
    def authenticate(self, username=None, password=None):
        try:
            user = Participant.objects.get(username=username)
            if user.check_password(password):
                return user
        except Participant.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return Participant.objects.get(pk=user_id)
        except Participant.DoesNotExist:
            return None

