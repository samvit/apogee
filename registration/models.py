from django.db import models
from django.contrib.auth.models import User, UserManager
from apogee.events.models import Event

class College(models.Model):
    name = models.CharField(max_length = 200)
    place = models.CharField(max_length = 100)
    gleader = models.ForeignKey('Participant', related_name='+',blank = True , null = True)
    def __unicode__(self):
        return self.name + ',' + self.place


class Participant(User):
    objects = UserManager()
    college = models.ForeignKey('College')
    gender = models.CharField(max_length = 7)
    contact_no = models.CharField(max_length = 30)
    events = models.ManyToManyField(Event , blank = True)
    gl = models.BooleanField(verbose_name = 'Group leader', blank = True)
    finalized = models.NullBooleanField(verbose_name = 'Finalise for apogee',null = True)
    shortlisted_events = models.ManyToManyField(Event , blank = True , related_name = 'shortlisted')
    draft = models.BooleanField(verbose_name = 'Draft recieved')
    year = models.IntegerField(blank=True, null=True)
    finalized_for =  models.ManyToManyField(Event , blank = True , related_name = 'finalised_for')
    def __unicode__(self):
        return self.first_name + " " + self.last_name+' - Username:  '+ self.username
