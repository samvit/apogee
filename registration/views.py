# Create your views here.
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.http import HttpResponseRedirect,Http404,HttpResponse
from django.utils import simplejson
from apogee.registration.models import Participant,College
from apogee.registration.forms import RegistrationForm,WorkshopForm,ForgotForm,EditProfileForm
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.db import IntegrityError
from apogee.sponsors.models import SponsorUpload
from apogee.events.models import EventCategory,Event
from django.core.mail import send_mail
from django.contrib import auth
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
import string
import random
import urllib, urllib2
from django.contrib.formtools.wizard import FormWizard
from django.core.exceptions import ObjectDoesNotExist
from apogee.updates.models import Update
#from apogee.registration.forms import YEAR_CHOICES
YEAR_CHOICES = (('1', 'First Year'), ('2', 'Second Year'), ('3', 'Third Year'), ('4', 'Fourth Year'), ('5', 'Fifth Year'), ('6', 'Post Graduate'))
def genpass():
    char_set = string.ascii_uppercase + string.digits
    return ''.join(random.sample(char_set,6))

def collegeadd():
    f = open('/usr/local/apogee/newcol.txt')
    for line in f:
        line = line.split(',')
        c = College()
        c.name = str(line[0])
        c.place = str(line[1])
        c.save()


@csrf_protect
def register(request):
    cats  = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    if request.POST:
        data = request.POST
        participant = Participant()
        form = RegistrationForm(data)
        password = genpass()
        
        if form.is_valid():
            p = Participant()
            p.username = data['username']
            p.set_password(password)
            p.email = data['e_mail']
            p.gender = data['gender']
            p.college = College.objects.get(pk = data['college'])
            p.contact_no = data['contact_no']
            p.first_name = data['first_name']
            p.last_name = data['last_name']
            p.year = int(data['year'])
            #if data['group_leader'] == 'y':
            #    p.gl = True
            #else:
            #    p.gl = False
            p.gl = False
            try:
                p.save()
            except IntegrityError:
                form.errors['username'] = 'Username already taken'
                return render_to_response('registration.html',{ 'form' : form ,'cats' : cats ,'updates':updates  },context_instance=RequestContext(request))
            message = render_to_string('e_mail.html',{'pass':password,'username':p.username})
            send_mail('Apogee 2012 account details', message , 'noreply@bits-apogee.org',[data['e_mail']], fail_silently=True)
            m = '1'
            return render_to_response('message.html',{'message' : m ,'cats' : cats,'updates':updates  },context_instance=RequestContext(request))
        return render_to_response('registration.html',{ 'form' : form,'cats' : cats,'updates':updates   },context_instance=RequestContext(request))
    form = RegistrationForm()
    return render_to_response('registration.html',{ 'form' : form ,'cats' : cats,'updates':updates },context_instance=RequestContext(request))

def gl(request):
    return render_to_response('gl.html')


@csrf_protect
def workshop(request):
    if request.POST:
       form = WorkshopForm(request.POST)
       if form.is_valid():
           print form
           message = ''
           message = form.cleaned_data['name'] +'\n'+'\n'+ form.cleaned_data['college'] +'\n'+ '\n'+form.cleaned_data['workshop_details'] +'\n'+'\n'+ form.cleaned_data['city'] +'\n'+'\n'+ form.cleaned_data['contact_no'] +'\n'+ '\n'+form.cleaned_data['e_mail']
           print message
           send_mail('Workshops Apogee 2012', message , 'workshops@bits-apogee.org',['samvit.1@gmail.com'], fail_silently=True)
           notice = 'Thank you! We will get back to you shortly'
    else:
        form = WorkshopForm()
        notice = ''
    sponsor_links = SponsorUpload.objects.values('image','width')
    cats  = EventCategory.objects.all().order_by('weight')
    return render_to_response('workshop.html',{ 'cats' : cats, 'sponsors': sponsor_links , 'form' : form , 'notice' : notice},context_instance=RequestContext(request))

@csrf_protect
def login(request):
    cats  = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    if request.user.is_authenticated() :
        return HttpResponseRedirect(reverse('profile'))
    if request.POST :
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username = username,password = password)
        if user is not None and user.is_active:
            auth.login(request,user)
            u = User.objects.get(username = username)
            return HttpResponseRedirect(reverse('profile'))
        else:
            message = 'User Does not exist/Wrong username-password !!!'
	    return render_to_response('login.html', {'message':message,'cats' : cats },context_instance=RequestContext(request))  
    else:
       return render_to_response('login.html',{'cats' : cats,'updates':updates },context_instance=RequestContext(request))


#def addevent(request):
#    if request.POST:
#        if request.user.is_authenticated():
#	    try :
#                p = Participant.objects.get(pk = request.user.id)
#                event = Event.objects.get(pk = request.POST['id'])
#                p.events.add(event)
#		return HttpResponse('1')
#            except ObjectDoesNotExist:
#                return HttpResponse('-1')
#    return HttpResponse('0')
    
@login_required
def addevent(request):
    if request.method == 'POST':
        try :
            p = Participant.objects.get(pk = request.user.id)
            event = Event.objects.get(pk = request.POST['id'])
        except ObjectDoesNotExist:
            return HttpResponse('-1')
        p.events.add(event)
        if event.name == 'Internship Competition':
            post_data = [('apiKey', '1d7be6b8705ab62f2a76'), ('reqType', 'addCompetitionParticipant'),
                         ('email', p.email), ('name', p.get_full_name()), ('mobile', p.contact_no),
                         ('college', p.college.name)]
            result = urllib2.urlopen('http://www.twenty19.com/external/create', urllib.urlencode(post_data))
            #content = result.read()
        return HttpResponse('1')
    return HttpResponse('0')


@login_required
def delevent(request):
    if request.POST:
        p =Participant.objects.get(pk = request.user.id)
        event = Event.objects.get(pk = request.POST['id'])
        p.events.remove(event)
	if event in p.shortlisted_events.all():
            p.shortlisted_events.remove(event)
	if event in p.finalized_for.all():
	    p.finalized_for.remove(event)
        return HttpResponse('<span style="color:red;">Event has been deleted</span>')
    return HttpResponse('Oops! we have encountered some error please try again later')



@login_required
def profile(request):
    cats  = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    year = ''
    try :
        p = Participant.objects.get(pk = request.user.id)
        for y in YEAR_CHOICES:
            if p.year == int(y[0]):
                year = y[1]
                break
    except ObjectDoesNotExist:
           return render_to_response('profile.html',{'p':request.user, 'cats' : cats,'updates':updates,'year':'Unknown year'},context_instance=RequestContext(request))
    return render_to_response('profile.html', {'p':p,'cats' : cats,'updates':updates, 'year':year },context_instance=RequestContext(request))


@login_required
def changepass(request):
    cats  = EventCategory.objects.all().order_by('weight')
    if request.POST:
        old_pass = request.POST['old']
        if request.user.check_password(request.POST['old']):
            if request.POST['new'] == request.POST['again']:
                user = User.objects.get(pk = request.user.id)
                user.set_password(request.POST['new'])
                user.save()
                p = Participant.objects.get(pk = request.user.id)
                return render_to_response('profile.html',{'success': 'Your password has been changed','cats' : cats,'p':p},context_instance=RequestContext(request))
            else:
                return render_to_response('changepass.html',{'error' : 'Both the passwords do not match','cats' : cats},context_instance=RequestContext(request))
        else:
             return render_to_response('changepass.html',{'error' : 'Old password does not match','cats' : cats},context_instance=RequestContext(request))
    return render_to_response('changepass.html',{'cats' : cats},context_instance=RequestContext(request))
  
def llogin(request):
    if request.POST :
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username = username,password = password)
        if user is not None and user.is_active:
            auth.login(request,user)
            message = '1'
            return render_to_response('llogin.html', {'message':message },context_instance=RequestContext(request))
        else:
            message = 'User Does not exist/Wrong username-password !!!'
            return render_to_response('llogin.html', {'message':message},context_instance=RequestContext(request))
    else:
       return render_to_response('llogin.html',context_instance=RequestContext(request))  

def forgotpass(request):
    cats  = EventCategory.objects.all().order_by('weight')
    if request.POST:
        try :
            p = Participant.objects.get(username = request.POST['username'])
        except ObjectDoesNotExist:
            form = ForgotForm()
            return render_to_response('forgotpass.html',{'emessage':'This username is not registered in the database, enter a valid one','form' : form ,'cats' : cats},context_instance=RequestContext(request))
        password = genpass()
        p.set_password(password)
        p.save()
        message = render_to_string('forgotmail.html',{'password' : password, 'participant':p})
        send_mail('Apogee account password reset', message , 'Apogee 2012<noreply@bits-apogee.org>',[p.email])
        return render_to_response('forgotpass.html',{'message':'Your password has been sent to your email address','cats' : cats},context_instance=RequestContext(request))
    form = ForgotForm()
    return render_to_response('forgotpass.html',{'form' : form ,'cats' : cats},context_instance=RequestContext(request)) 

from django.http import HttpResponse
from django.core.urlresolvers import reverse
@login_required
def editprofile(request):
    cats = EventCategory.objects.all().order_by('weight')
    updates = Update.objects.all().order_by('-timestamp')
    try: # Sanity check
        p = Participant.objects.get(pk = request.user.id)
    except ObjectDoesNotExist:
        raise Http404
    if request.method == 'POST':
        form = EditProfileForm(request.POST)
	if form.is_valid():
	    p.contact_no = form.cleaned_data['contact_no']
	    p.first_name = form.cleaned_data['first_name']
	    p.last_name = form.cleaned_data['last_name']
	    p.email = form.cleaned_data['e_mail']
            p.year = int(form.cleaned_data['year'])
	    p.save()
	else:
	    return render_to_response('editprofile.html', {'form' : form, 'cats' : cats},
				      context_instance=RequestContext(request))
	return HttpResponseRedirect(reverse('profile'))
    else:
	form = EditProfileForm({'contact_no':p.contact_no, 'first_name':p.first_name, 'last_name':p.last_name, 'e_mail':p.email, 'year':p.year})
        return render_to_response('editprofile.html', {'form' : form, 'cats' : cats},
				  context_instance=RequestContext(request))
