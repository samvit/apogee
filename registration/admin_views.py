from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.contrib.admin.views.decorators import staff_member_required
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
import hashlib

from apogee.events.models import Event, EventCategory
from apogee.registration.models import Participant

#TODO:
#Make the list of  colleges show up as a select box
#Mobile redirection


@staff_member_required
def addparticipants(request, eventid):
    event = Event.objects.get(id=eventid)
    if request.user != event.author and  not request.user.is_superuser:
        raise Http404
    if request.method == 'POST':
        participant_ids = request.POST.getlist('participants')
        if request.POST.get('shortlist', None) is not None:
            for id in participant_ids:
                participant = Participant.objects.get(id=id)
                participant.shortlisted_events.add(event)
                participant.save()
        elif request.POST.get('unshortlist', None) is not None:
            for id in participant_ids:
                participant = Participant.objects.get(id=id)
                participant.shortlisted_events.remove(event)
        elif request.POST.get('finalize', None) is not None and request.user.is_superuser:
            for id in participant_ids:
                participant = Participant.objects.get(id=id)
                participant.finalized_for.add(event)
                participant.finalized = True # HAS TO BE THERE
                participant.save()
                ucid = hashlib.sha224(str(participant.id)).hexdigest()[:7]
                events = participant.finalized_for.exclude(category__name='Workshops')
                papers = participant.paperupload_set.filter(finalized=True)
                projects = participant.projectupload_set.filter(finalized=True)
                workshops = participant.finalized_for.filter(category__name='Workshops')
                try:
                    roboarm_workshop = workshops.get(name='RoboARM Workshop')
                except ObjectDoesNotExist:
                    roboarm_workshop = None

                try:
                    deltawing_workshop = workshops.get(name='Delta Wing Aircraft Workshop')
                except ObjectDoesNotExist:
                    deltawing_workshop = None
                email = render_to_string('email_templates/general.html',
                                         {'participant':participant, 'ucid':ucid, 'events':events, 'papers':papers, 'projects':projects, 'workshops':workshops, 'roboarm_workshop':roboarm_workshop, 'deltawing_worksop':deltawing_workshop},
                                         context_instance=RequestContext(request))
                send_mail('Apogee 2012 Confirmation', email, 'Apogee 2012 <noreply@bits-apogee.org>', [participant.email])
        else:
            raise Http404
        return HttpResponseRedirect(reverse('view-shortlist'))
    finalized = event.finalised_for.all()
    shortlisted = [x for x in event.shortlisted.all() if x not in finalized]
    # All the participants that haven't been shortlisted yet, but have registered for this event
    registered = [x for x in event.participant_set.all() if x not in shortlisted and x not in finalized]
    return render_to_response('admin/shortlist_applicants.html',
                              {'event':event, 'shortlisted':shortlisted, 'registered':registered, 'finalized':finalized},
                              context_instance=RequestContext(request))

@staff_member_required
def eventlist(request):
    return render_to_response('admin/shortlist.html', {'categories':EventCategory.objects.all()},
                              context_instance=RequestContext(request))
@staff_member_required
def event_mailer(request):
    if not request.user.is_superuser:
        raise Http404
    event_list = Event.objects.filter(register=True).order_by('category__name')
    if request.method == 'POST':
        email_content = request.POST.get('email-content', None)
        email_subject = request.POST.get('email-subject', None)
        if email_content is None or email_subject is None:
            raise Http404('Subject and/or content field is empty')
        event_ids = request.POST.getlist('events')
        email_list = []
        for id in event_ids:
            event = Event.objects.get(id=id)
            participants = event.finalised_for.all()
            for participant in participants:
                email_list.append(participant.email)
        try:
            send_mail(email_subject, email_content, 'APOGEE 2012 <noreply@bits-apogee.org>', email_list, fail_silently=False)
        except:
            raise Http404('Some error has occurred. Please contact the system administrator.')

        return render_to_response('admin/email_participants.html', {'event_list':event_list, 'mails_sent':len(email_list)},
                                  context_instance=RequestContext(request))
            
    return render_to_response('admin/email_participants.html', {'event_list':event_list},
                              context_instance=RequestContext(request))



def event_to_string(events):
  s = ''
  for event in events:
    s+=str(event.name) + ', '
  return s[:-1]


@staff_member_required
def event_sheet(request,eventid):
  if not request.user.is_superuser:
    raise Http404
  import xlwt
  book = xlwt.Workbook()
  sheet = book.add_sheet('participants_sheet_full')
  style = xlwt.easyxf('font: name Sans-Serif, color-index blue, bold on')
  sheet.write(0,0,'Username',style = style)
  sheet.write(0,1,'Full Name',style = style)
  sheet.write(0,2,'Gender',style = style)
  sheet.write(0,3,'College',style = style)
  sheet.write(0,4,'Contact',style = style)
  sheet.write(0,5,'Email',style = style)
  sheet.write(0,6,'APOGEE Confirmation code',style = style)
  sheet.write(0,7,'Registered Events',style = style)
  sheet.write(0,8,'Shortlisted Events',style = style)
  sheet.write(0,9,'Finalized Events',style = style)
  sheet.write(0,10,'Finalized for APOGEE',style = style)
  
  row = 1
  event = Event.objects.get( pk = eventid)
  participants = Participant.objects.filter(events = event)

  for participant in participants:
    sheet.write(row,0,participant.username)
    sheet.write(row,1,participant.get_full_name())
    sheet.write(row,2,participant.gender)
    sheet.write(row,3,str(participant.college))
    sheet.write(row,5,participant.email)
    sheet.write(row,4,participant.contact_no)
    sheet.write(row,6,hashlib.sha224(str(participant.id)).hexdigest()[:7])
    sheet.write(row,7,event_to_string(participant.events.all()))
    sheet.write(row,8,event_to_string(participant.shortlisted_events.all()))
    sheet.write(row,9,event_to_string(participant.finalized_for.all()))
    sheet.write(row,10,participant.finalized)
    row+=1
  response = HttpResponse(mimetype='application/vnd.ms-excel')
  response['Content-Disposition'] = 'attachment; filename=participants_'+event.name+'.xls'
  book.save(response)
  return response
  
  
@staff_member_required
def participant_sheet(request):
  if not request.user.is_superuser:
    raise Http404
  import xlwt
  book = xlwt.Workbook()
  sheet = book.add_sheet('participants_sheet_full')
  style = xlwt.easyxf('font: name Sans-Serif, color-index blue, bold on')
  sheet.write(0,0,'Username',style = style)
  sheet.write(0,1,'Full Name',style = style)
  sheet.write(0,2,'Gender',style = style)
  sheet.write(0,3,'College',style = style)
  sheet.write(0,4,'Contact',style = style)
  sheet.write(0,5,'Email',style = style)
  sheet.write(0,6,'APOGEE Confirmation code',style = style)
  sheet.write(0,7,'Registered Events',style = style)
  sheet.write(0,8,'Shortlisted Events',style = style)
  sheet.write(0,9,'Finalized Events',style = style)
  sheet.write(0,10,'Finalized for APOGEE',style = style)
  
  row = 1
  participants = Participant.objects.all()

  for participant in participants:
    sheet.write(row,0,participant.username)
    sheet.write(row,1,participant.get_full_name())
    sheet.write(row,2,participant.gender)
    sheet.write(row,3,str(participant.college))
    sheet.write(row,5,participant.email)
    sheet.write(row,4,participant.contact_no)
    sheet.write(row,6,hashlib.sha224(str(participant.id)).hexdigest()[:7])
    sheet.write(row,7,event_to_string(participant.events.all()))
    sheet.write(row,8,event_to_string(participant.shortlisted_events.all()))
    sheet.write(row,9,event_to_string(participant.finalized_for.all()))
    sheet.write(row,10,participant.finalized)
    row+=1
  response = HttpResponse(mimetype='application/vnd.ms-excel')
  response['Content-Disposition'] = 'attachment; filename=participants_all.xls'
  book.save(response)
  return response
