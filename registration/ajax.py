from apogee.registration.models import College
from django.db.models import Q

class SelectCollege(object):

      def get_query(self,q,request):
        return College.objects.filter(Q(name__icontains = q) | Q(place__icontains = q))

      def format_result(self,College):
        return College.name+', '+College.place

      def format_item(self,College):
        return College.name+', '+College.place

      def get_objects(self,ids):
        return College.objects.filter(pk__in=ids).order_by('name')



