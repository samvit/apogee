from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from apogee.registration.models import Participant,College
from apogee.events.models import Event
from django.core.mail import send_mail
from django.template.context import RequestContext
from django.template.loader import render_to_string
import hashlib




class ParticipantAdmin(admin.ModelAdmin):
    list_display = ['username','get_full_name','College','event_list','shortlisted','finalized_events','finalized']
    filter_horizontal = ('events','shortlisted_events',)

    def invite(self,request,queryset):
      rows_updated = queryset.update(finalized = True)
      for participant in queryset:
        apid = hashlib.sha224(str(participant.id)).hexdigest()[:7]
        email = render_to_string('confirm_none.html',{'participant':participant,'apid':apid})
        send_mail('Apogee 2011 confirmation mail ',email,'Apogee 2011 <noreply@bits-apogee.org>',['shantanab@bits-apogee.org','samvit.1@gmail.com'],fail_silently = True)
      if rows_updated == 1:
        message_bit = "1 Participant was"
      else:
        message_bit = "%s Participants were" % rows_updated
      self.message_user(request, "%s invited to Apogee 2011. Emails sent." % message_bit)
    invite.short_description = "Mark selected to invite to Apogee"

    def finalize_silent(self,request,queryset):
        rows_updated = queryset.update(finalized = True)
        if rows_updated == 1:
            message_bit = "1 Participant was"
        else:
            message_bit = "%s Participants were" % rows_updated
        self.message_user(request, "%s successfully finalised for Apogee 2012. NO Emails sent." % message_bit)
    finalize_silent.short_description = "Mark selected as finalised - NO EMAIL"

    def finalise(self, request, queryset):
        rows_updated = queryset.update(finalized = True)
	for participant in queryset:
            ucid = hashlib.sha224(str(participant.id)).hexdigest()[:7]
            events = participant.finalized_for.exclude(category__name='Workshops')
            papers = participant.paperupload_set.filter(finalized=True)
            projects = participant.projectupload_set.filter(finalized=True)
            workshops = participant.finalized_for.filter(category__name='Workshops')
            email = render_to_string('email_templates/general.html',
                                         {'ucid':ucid, 'events':events, 'papers':papers, 'projects':projects, 'workshops':workshops, 'participant':participant},
                                         context_instance=RequestContext(request))
            send_mail('Apogee 2012 Confirmation', email, 'Apogee 2012 <noreply@bits-apogee.org>', [participant.email])
        if rows_updated == 1:
            message_bit = "1 Participant was"
        else:
            message_bit = "%s Participants were" % rows_updated
        self.message_user(request, "%s successfully finalised for Apogee 2012. Emails sent." % message_bit)
    finalise.short_description = "Mark selected as finalised"
    def reject(self, request, queryset):
        rows_updated = queryset.update(finalized = False)
        if rows_updated == 1:
            message_bit = "1 Participant was"
        else:
            message_bit = "%s Participants were" % rows_updated
        self.message_user(request, "%s rejected for on campus Apogee 2011." % message_bit)
    reject.short_description = "Mark selected as rejected for on campus events"

    def reset_status(self, request, queryset):
        rows_updated = queryset.update(finalized = None)
        if rows_updated == 1:
            message_bit = "Status of 1 participant was"
        else:
            message_bit = "Status of %s participants were" % rows_updated
        self.message_user(request, "%s reset " % message_bit)
    reset_status.short_description = "Reset Finalised status"

    def shortlisted(self,p):
        events = p.shortlisted_events.all().values_list()
        u = '<ul>'
        for event in events:
            u += '<li>'+event[1]+'</li>'
        u+='</ul>'
        return u
    shortlisted.allow_tags = True
    
    def finalized_events(self,p):
        events = p.finalized_for.all().values_list()
        u = '<ul>'
        for event in events:
            u += '<li>'+event[1]+'</li>'
        u+='</ul>'
        return u
    finalized_events.allow_tags = True
        
    def event_list(self, p):
        events =  p.events.all().values_list()
        u = '<ul>'
        for event in events:
            u += '<li>'+event[1]+'</li>'
        u+='</ul>'
        return u
    event_list.allow_tags = True

    def College(self,p):
        return str(p.college)

    def queryset(self , request):
        qs = super(ParticipantAdmin , self).queryset(request)
        if request.user.is_superuser or request.user.username == 'bitspcr':
            return qs
        events = Event.objects.filter( author = request.user)
        return qs.filter(events__in  = events).distinct()

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        p = self.get_object(request, Participant)
        self.filter_horizontal= ('events','shortlisted_events',)
        if db_field.name == "shortlisted_events":
            kwargs["queryset"] = p.events.all()
        return super(ParticipantAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_object(self, request, model):
        object_id = request.META['PATH_INFO'].strip('/').split('/')[-1]
        try:
            object_id = int(object_id)
        except ValueError:
            return None
        return Participant.objects.get(pk=object_id)
    def get_actions(self, request):
        actions = super(ParticipantAdmin, self).get_actions(request)
        if request.user.is_superuser:
       	    return actions
        else:
       	    del actions['finalise']
       	    del actions['finalize_silent']
       	    del actions['reject']
 	    del actions['reset_status']
	    #del actions['invite']
       	    return actions
    actions = [finalise,reject,reset_status,finalize_silent]
    fields = ('username','first_name','last_name','email','college','gender','contact_no','events','gl','shortlisted_events','draft')
    list_filter = ('finalized','events','shortlisted_events','college')
    readonly_fields = ['shortlisted_events','finalized','gl']
    search_fields = ['username','first_name','last_name']

class CollegeAdmin(admin.ModelAdmin):
     list_filter = ['place']
     def formfield_for_foreignkey(self, db_field, request, **kwargs):
        college = self.get_object(request, College)
        if db_field.name == "gleader":
            kwargs["queryset"] = Participant.objects.filter(gl = True , college = college)
        return super(CollegeAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

     def get_object(self, request, model):
        object_id = request.META['PATH_INFO'].strip('/').split('/')[-1]
        try:
            object_id = int(object_id)
        except ValueError:
            return None
        print object_id
        return College.objects.get(pk=object_id)

admin.site.register(Participant,ParticipantAdmin)
admin.site.register(College,CollegeAdmin)