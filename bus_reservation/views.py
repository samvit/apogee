# Create your views here.
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect,Http404,HttpResponse
from django.template import RequestContext
from django.core.exceptions import ObjectDoesNotExist
from apogee.registration.models import Participant
from apogee.bus_reservation.forms import BookingForm
from apogee.bus_reservation.models import Booking

@csrf_protect
@login_required
def bookbus(request):
  try:
    p = Participant.objects.get( pk = request.user)
  except :
    return render_to_response('book_bus.html',context_instance=RequestContext(request))
  if request.POST:
    form = BookingForm(request.POST)
    if form.is_valid():
      r = Booking()
      r.bus = form.cleaned_data['bus']
      try :
        r.participant = p
      except:
        form.errors['bus'] = ['You are not logged in as a participant']
        return render_to_response('book_bus.html',{'form' : form , 'participant' : p},context_instance=RequestContext(request))
      r.save()
      message = 'Your seat has been reserved.'
      return render_to_response('book_bus.html',{'form' : form, 'participant': p ,'message':message},context_instance=RequestContext(request))
    else:
      return render_to_response('book_bus.html',{'form' : form, 'participant' : p},context_instance=RequestContext(request))
  else:
    form = BookingForm()
    return render_to_response('book_bus.html', {'form' : form, 'participant' : p},context_instance=RequestContext(request))
    
    
@login_required
def delbooking(request):
  if request.POST:
        if request.POST.has_key('id'):
            try :
                booking = Booking.objects.get( pk = request.POST['id'])
                booking.delete()
                return HttpResponse('Your booking has been deleted')
            except ObjectDoesNotExist:
                return HttpResponse('Some error has occured!')
  return HttpResponse('Some error has occured!')
