from django.db import models
from apogee.registration.models import Participant
# Create your models here.

class Station(models.Model):
  name = models.CharField(max_length = 100)
  def __unicode__(self):
    return self.name


class Bus(models.Model):
  time = models.DateTimeField()
  station = models.ForeignKey(Station)
  operational = models.BooleanField( default = False)  
  
  class Meta:
    verbose_name_plural = 'Buses'
    
  def __unicode__(self):
    return str(self.station) + ' -- '+str(self.time.ctime())
  

class Booking(models.Model):
  participant = models.ForeignKey(Participant)
  bus = models.ForeignKey(Bus)
  
  def __unicode__(self):
    return self.participant.username
  
	
