from django.contrib import admin
from apogee.bus_reservation.models import Bus,Station,Booking

class BookingAdmin(admin.ModelAdmin):
  list_display = ['participant','bus']
  list_filter = ['bus']

class BusAdmin(admin.ModelAdmin):
  list_display = ['station','time','bookings']
  
  def bookings(self,bus):
    return bus.booking_set.count()

admin.site.register(Bus,BusAdmin)
admin.site.register(Station)
admin.site.register(Booking,BookingAdmin)
