from django import forms
from apogee.bus_reservation.models import Booking
from apogee.bus_reservation.models import Bus

class BookingForm(forms.ModelForm):
  bus = forms.ModelChoiceField(queryset= Bus.objects.filter(operational = True))
  class Meta:
    model = Booking
    exclude = ('participant')
    
