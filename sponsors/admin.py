from django.contrib import admin
from apogee.sponsors.models import  SponsorUpload,Category

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'weight', )

class SponsorUploadAdmin(admin.ModelAdmin):
    exclude = ('width',)

admin.site.register(SponsorUpload,SponsorUploadAdmin)
admin.site.register(Category, CategoryAdmin)
