# Create your views here.
from apogee.sponsors.models import SponsorUpload,Category
from django.shortcuts import render_to_response
from django.template import RequestContext
from apogee.events.models import EventCategory

def sponsors(request):
    cats  = EventCategory.objects.all().order_by('weight')
    s = SponsorUpload.objects.all().order_by('category__weight')
    c = Category.objects.all().order_by('weight')
    return render_to_response('sponsors.html',{'sponsors': s , 'categories' : c ,'cats' : cats},context_instance=RequestContext(request))
