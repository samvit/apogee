from django.db import models

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length = 100)
    weight = models.IntegerField()

    def __unicode__(self):
        return self.name

class SponsorUpload(models.Model):
    name = models.CharField(max_length = 100)
    category = models.ForeignKey(Category)
    width = models.IntegerField(null = True)
    image = models.ImageField(upload_to = 'images/sponsors',blank='True',width_field='width')
    link = models.URLField(verify_exists = False)


    def __unicode__(self):
        return self.name

'''class Link(models.Model):
    sponsor = models.OneToOneField(SponsorUpload)
    url = models.SlugField(max_length = 200)

    def __unicode__(self):
        return str(self.sponsor)'''
