from django.conf.urls.defaults import *
from apogee.events import views
from django.shortcuts import render_to_response
from django.contrib import admin
from django.contrib.auth.views import logout
from apogee.registration.views import *
from apogee.uploads.views import *
from apogee.teamreg.views import *
from apogee.sponsors.views import *
from apogee.updates.views import *
from apogee.bus_reservation.views import *
import admin_tools
import ajax_select
admin.autodiscover()

urlpatterns = patterns('apogee.registration.views',

  url(r'^register/$',register,name = 'register'),
  url(r'^login/$',login,name = 'login'),
  url(r'^profile/$',profile,name = 'profile'),
  url(r'^addevent/$',addevent),
  url(r'^delevent/$',delevent),
  url(r'llogin/$',llogin,name = 'llogin'),
  url(r'^changepass/$',changepass , name ='changepass'),
  url(r'^gl/$',gl),
  url(r'^forgotpass/$',forgotpass,name = 'forgot'),
  url(r'^editprofile/$',editprofile, name = 'editprofile'),
  url(r'^massmail/$',mass_mail, name = 'mass_email'),
)

urlpatterns += patterns('apogee.registration.admin_views',
    url(r'^admin/shortlist/(?P<eventid>\d+)/$', 'addparticipants', name='add-shortlist'),
    url(r'^admin/shortlist/$', 'eventlist', name='view-shortlist'),
	url(r'^admin/eventmailer/$', 'event_mailer', name='event-mailer'),
	url(r'^admin/gen_xl_sheet/(?P<eventid>\d+)/$', 'event_sheet', name='event-sheet'),
	url(r'^admin/gen_participant_sheet/$', 'participant_sheet', name='participants-sheet'),
)
handler404 = 'apogee.events.views.view404'
"""
  MOBILE SITE URLS

"""
urlpatterns += patterns('apogee.events.views',
    (r'^mobile/$', 'mob_index'),
    (r'^mobile/(?P<event>[^/]+)/$', 'mob_events'),
    (r'^mobile/(.+)/(?P<eventPage>.+)/$', 'mob_eventPage'),
)
urlpatterns += patterns('',

            url(r'^teamcreate/$',team_create,name = 'create_team'),
            url(r'^jointeam/$',join_team,name='join_team'),
            url(r'^remove_me/$',remove_me,name = 'remove_me'),
            url(r'^sponsor/$',sponsors,name = 'sponsor'),
	    url(r'^webteam/$',webteam, name ='webteam'),
)

urlpatterns += patterns('apogee.uploads.views',
  url(r'^upload/$',paperupload,name = 'paperupload',),
  url(r'^delupload/$',delpaperupload,name = 'delpaperupload',),
   url(r'^prupload/$',projectupload,name = 'projectupload',),
  url(r'^delprupload/$',delprojectupload,name = 'delprojectupload',),
    url(r'^virupload/$', virtuosityupload, name='virupload'),
  url(r'^delvirupload/$', delvirtuosityupload, name='delvirupload'),
  #url(r'^paperxl/$',paperxl,name = 'paperxl',),
   #url(r'^papers/$',papers,name = 'papers',),
)

#urlpatterns += patterns('',
#  url(r'^bookbus/$',bookbus,name = 'bookbus'),
#  url(r'^delbooking/$',delbooking,name = 'delbooking'),
#
#)

urlpatterns += patterns('',
    # Example:
    # (r'^apogee/', include('apogee.foo.urls')),

    # Uncomment the admin/doc line below and add 'django.contrib.admindocs'
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^$',views.home,name='home'),
    url(r'^logout/$',logout,{'template_name':'logout.html','next_page':'/'},name ='logout'),
    url(r'^getevents/$',views.getevents,name = 'getevents'),
    #url(r'^getimg/$',views.getimg),
    url(r'^getcontent/$',views.getcontent,name = 'getcontent'),
    #url(r'^contacts/$',views.contacts,name = 'contacts'),
    #url(r'^workshop/$',views.workshop),
    #url(r'^participation/$',views.participation , name = 'participation'),
    #url(r'^event/(.*)/$',views.facebook),
    url(r'^search/$',views.search,name = 'search'), 
    #url(r'^downloads/$',views.downloads, name='downloads' ),
    url(r'^admin_tools/', include('admin_tools.urls')),
    (r'^ajax_select/', include('ajax_select.urls')),
    url(r'^admin/', include(admin.site.urls)),

    (r'^site_media/(?P<path>.*)$','django.views.static.serve',
         {'document_root':'media/'}),
    url(r'^(.+)/(.+)/$', views.display_event),
    url(r'^(.+)/$', views.home_view),
)

urlpatterns += patterns('',
#	url(r'^updates/$',updates, name = 'update'),
	url(r'^massmail/$',mass_mail, name = 'mass_email'),

)



from apogee import settings
if settings.DEBUG:
    urlpatterns  = patterns('',
                            url(r'^2012/', include(urlpatterns)),
    )

    from django.views.static import serve
    _media_url = settings.MEDIA_URL
    if _media_url.startswith('/'):
        _media_url = _media_url[1:]
        urlpatterns += patterns('',
            (r'^%s(?P<path>.*)$' % _media_url,
            serve,
            {'document_root': settings.MEDIA_ROOT}))
    del(_media_url, serve)
