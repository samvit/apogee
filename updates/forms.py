from django import forms


class EmailForm(forms.Form):
	subject = forms.CharField( max_length = 100)
	text = forms.CharField( widget = forms.Textarea ,max_length = 10000000)
	html = forms.CharField( widget = forms.Textarea ,max_length = 10000000)
	