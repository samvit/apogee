from django.contrib import admin
from apogee.updates.models import Update

class UpdateAdmin(admin.ModelAdmin):
	list_display = ('update','timestamp')

admin.site.register(Update,UpdateAdmin)