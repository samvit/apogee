# Create your views here.
from django.core.exceptions import ObjectDoesNotExist
from django.utils import simplejson
from apogee.updates.models import Update
from django.http import HttpResponse,Http404
from django.core import serializers
from django.core.mail import EmailMultiAlternatives
from django.core import mail
from apogee.updates.forms import EmailForm
from apogee.registration.models import Participant
from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext

def updates(request):
	data = serializers.serialize("json", Update.objects.all().order_by('-timestamp'))
	return HttpResponse(data,mimetype='application/json')
	
@csrf_protect	
def mass_mail(request):
	if request.user.is_superuser:
		if request.POST and request.POST.has_key('text') and request.POST.has_key('text'):
			if not (len(request.POST['subject']) and len(request.POST['text'])):
				form = EmailForm()
				form.error = 'Some fields are left blank'
				return render_to_response('m_email.html', { 'form' : form },context_instance=RequestContext(request))
			connection = mail.get_connection()
			emails = Participant.objects.values('email')
			bcc_list = []
			for email in emails:
				bcc_list.append(str(email['email']))
		
			while(len(bcc_list)):
                                connection.open()
				email = EmailMultiAlternatives(request.POST['subject'], request.POST['text'], 'APOGEE 2012 <noreply@bits-apogee.org>',
			            [''],bcc_list[:200],
			            )
				email.attach_alternative(request.POST['html'],"text/html")
				email.content_subtype = "html"
				connection.send_messages([email])
				bcc_list = bcc_list[200:]
			        connection.close() 
			form = EmailForm()
			return render_to_response('m_email.html', { 'form' : form ,'messages' : ['Emails sent']},context_instance=RequestContext(request))
		else:
						
			form = EmailForm()
			return render_to_response('m_email.html', { 'form' : form },context_instance=RequestContext(request))
	else:
		raise Http404
		
