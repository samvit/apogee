from django.db import models

# Create your models here.

class Update(models.Model):
	update = models.CharField( max_length = 200)
	timestamp = models.DateTimeField( auto_now_add = True )
	
	def __unicode__(self):
		return self.update
		
		
